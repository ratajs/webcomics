# Transcript of Pepper&Carrot Episode 26 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 26: Books Are Great

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Books are great!
Pepper|3|False|This is pure gold!
Pepper|2|True|This rare book contains the notes of a skilled adventurer and describes everything about this dungeon.
Pepper|4|False|For example: this evil eye on the front door shoots deadly fireballs to ward off unauthorized visitors.
Pepper|5|False|But if you carefully approach it from the side, out of view...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...and then cover the eye with a hat...
Pepper|3|False|...the door should open by itself!
Pepper|4|True|They even recommend leaving the hat in place to make an easier escape!
Pepper|5|False|Books are so great!
Writing|6|False|“Jump directly over the first balcony.”
Pepper|7|False|OK!
Sound|9|False|BOING
Sound|10|False|BOING
Writing|8|False|“You may notice a falling sensation. This is normal. This is a shortcut.”
Writing|11|False|“Enjoy the comfort of the illuminated web without any risk: these spiders are photosynthetic and feed off of the reflected light.”
Writing|12|False|“I recommend a quick nap to restore all your strength.”
Pepper|13|False|I so love books!
Sound|2|False|Poof!
<hidden>|0|False|This “they” means one person, without specifying their gender. If possible, please try to do the same in your language (suggestion: if your language doesn't have genderless pronouns, maybe you can translate as “the author”). If that doesn't really work well, assume the author is a woman.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|I know how much cats don't like to get wet.
Pepper|3|True|Don't worry!
Pepper|4|False|Books are also good for that!
Pepper|5|True|And in less than ten minutes from entering the dungeon, I'm sitting in the treasure room picking leaves off of the rare Water-Tree.
Pepper|6|False|Books are magical!
Pepper|1|True|Oh, Carrot;

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|3|False|“Because you took the shortcut you should now be entering the room of The Guardian.”
Pepper|4|True|Hmm...
Pepper|5|True|the big boss?!?
Pepper|6|False|But where?
Pepper|7|True|AH HA...
Pepper|8|False|Found you!
Pepper|11|True|Pfff...!
Pepper|12|False|Too easy when you know the weak point!
Writing|1|False|“Pick up a feather along the way.”
Pepper|2|False|There's no lack of feathers here!
Pepper|9|True|tickle
Pepper|10|False|tickle

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Shkrrkrr !|nowhitespace
Pepper|2|False|Aha! Here's the exit!
Sound|3|False|Wooop!
Pepper|5|True|Ewww...
Pepper|6|False|I'm sure there's something in here about a way to remove this “thing”...
Pepper|7|True|Carrot... Can you read it?
Pepper|8|True|Mm... of course you can't...
Pepper|9|False|Meh...
Pepper|4|False|?!
Pepper|12|False|Grrr!!!
Pepper|10|True|I KNEW IT!!!
Pepper|11|True|It was too good to be true!
Pepper|13|False|Maybe books can't handle every situation after all...
Sound|14|False|Paf!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...unless
Pepper|2|False|YAA A A A A A A ! ! !|nowhitespace
Sound|3|False|P OW !|nowhitespace
Pepper|5|False|...books are great!
Pepper|4|True|Nope, it's true; for every situation...
Narrator|6|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|License: Creative Commons Attribution 4.0. Software: Krita 4.1, Inkscape 0.92.3 on Kubuntu 17.10. Art & scenario: David Revoy. Script doctoring: Craig Maloney. Beta readers: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. English version Proofreading: Craig Maloney, CalimeroTeknik. Based on the universe of Hereva Creation: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28 July 2018 www.peppercarrot.com
Credits|3|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
Credits|2|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 1098 Patrons:
<hidden>|0|False|Remove the names of the people who helped to create the English version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
