# Transcript of Pepper&Carrot Episode 27 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 27 : L'Invention de Coriandre

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|2|True|COURONNEMENT DE SA MAJESTÉ
Narrateur|1|False|Qualicity, dans l'après-midi
Tailleur|4|False|S'il vous plaît , Princesse Coriandre...
Tailleur|5|False|Pourriez-vous arrêter de bouger autant ?
Coriandre|6|False|D'accord, d'accord...
Tailleur|7|False|Il nous reste peu de temps et le couronnement commence dans trois heures.
Coriandre|8|False|...
Écriture|3|False|LA REINE CORIANDRE
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|1|True|Vous savez...
Coriandre|2|False|je pensais encore à la sécurité...
Coriandre|3|True|Tant de jeunes reines et rois ont été assassinés lors de leur couronnement.
Coriandre|4|False|Ma famille n'a pas été épargnée.
Coriandre|5|True|Et maintenant, dans quelques heures, nous recevrons des milliers d'invités venant du monde entier, et je serai le centre d'attention.
Pepper|7|True|Ne t'inquiète pas, Coriandre.
Coriandre|6|False|J'ai vraiment, vraiment peur !
Pepper|8|True|On va suivre notre plan.
Pepper|9|False|Shichimi et moi serons tes gardes du corps, tu te souviens ?
Shichimi|10|True|Exactement.
Shichimi|11|False|Tu devrais faire une pause et penser à autre chose.
Coriandre|12|False|OK.
Coriandre|13|False|Hé ! Et si je vous montrais ma dernière invention ?
Tailleur|14|False|...
Coriandre|15|True|C'est en bas, dans mon atelier.
Coriandre|16|False|Suivez-moi.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|1|False|Vous allez bientôt comprendre pourquoi je voulais construire celui-ci.
Coriandre|2|False|Ça concerne mes inquiétudes au sujet du couronnement.
Coriandre|3|False|Laissez-moi vous présenter ma nouvelle invention...
Robot Psychologue|5|False|Hello, world !
Robot Psychologue|6|False|Je suis votre ROBOT PSYCHOLOGUE. Bip, Bip !
Robot Psychologue|7|False|Veuillez vous allonger sur mon divan pour une séance gratuite.
Son|4|False|Clic !
Son|8|False|Tap !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandre|14|False|Il envoie une décharge électrique dès qu'il détecte qu'on ment .
Coriandre|1|True|Alors, vous en dites quoi ?
Coriandre|2|False|Vous voulez essayer ?
Pepper|3|True|Bah.
Pepper|4|True|Pas besoin.
Pepper|5|False|Je n'ai aucun problème psychologique.
Son|17|True|G
Pepper|11|False|HÉ, C'EST QUOI LE PROBLÈME AVEC TON ROBOT ?!
Coriandre|12|True|Désolée !
Coriandre|13|False|Je n'arrive toujours pas à corriger ce bug.
Pepper|15|False|Mentir ?!
Pepper|16|False|Mais je n'ai jamais menti de toute ma VIE !
Pepper|22|True|C'EST...
Pepper|23|True|PAS...
Pepper|24|False|DRÔLE !!!
Son|18|True|Z|nowhitespace
Son|19|True|Z|nowhitespace
Son|20|True|T|nowhitespace
Son|21|False|!|nowhitespace
Son|6|True|G
Son|7|True|Z|nowhitespace
Son|8|True|Z|nowhitespace
Son|9|True|T|nowhitespace
Son|10|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Drôle ? Non, bien sûr que non ! Désolée.
Coriandre|2|False|Hi, hi ! C'est vrai ! Pas drôle du tout !
Pepper|8|True|Ha...
Pepper|9|True|Ha...
Pepper|10|False|Ha.
Coriandre|11|True|OH NON !
Coriandre|12|False|Ma robe pour la cérémonie est fichue ! Mon tailleur va être furieux !
Coriandre|13|True|Vilain robot !
Coriandre|14|False|Je vais corriger ton bug pour de bon !
Pepper|15|False|ATTENDS !!!
Son|3|True|G
Son|4|True|Z|nowhitespace
Son|5|True|Z|nowhitespace
Son|6|True|T|nowhitespace
Son|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|4|True|COURONNEMENT DE SA MAJESTÉ
Pepper|1|False|J'ai ma petite idée pour exploiter ce bug.
Son|2|False|Ting !
Narrateur|3|False|Dans la soirée...
Coriandre|11|False|Pepper, tu es une génie !
Pepper|12|True|Non, c'est toi.
Pepper|13|False|C'est ton invention !
Robot Psychologue|14|False|Suivant, je vous prie.
Robot Psychologue|15|True|Bonsoir,
Robot Psychologue|16|False|Prévoyez-vous un assassinat ce soir ?
Narrateur|17|False|À SUIVRE...
Son|6|True|G|nowhitespace
Son|7|True|Z|nowhitespace
Son|8|True|Z|nowhitespace
Son|9|False|T|nowhitespace
Écriture|10|False|!
Écriture|5|False|LA REINE CORIANDRE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 1060 mécènes !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ...et d'autres !
Pepper|8|False|Merci !
Pepper|2|True|Le saviez-vous ?
Crédits|1|False|31 Octobre 2018 Art & scénario : David Revoy. Lecteurs de la version bêta : Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Version française Traduction : Calimeroteknik, Nicolas Artance, Midgard, Valvin . Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Écrivains : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita 4.2-dev, Inkscape 0.92.3 sur Kubuntu 18.04. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
