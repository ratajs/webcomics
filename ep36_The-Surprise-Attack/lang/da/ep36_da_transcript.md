# Transcript of Pepper&Carrot Episode 36 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 36: Overraskelsesangrebet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|UNDSKYLD?!
Wasabi|2|False|Laver du sjov?!
Wasabi|3|False|SMID DET DÉR I FÆNGSEL MED DET SAMME!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Psch h...
Pepper|2|True|Pokkers!
Pepper|3|False|Jeg kan jo ikke gøre noget her!
Pepper|4|False|Forbandede magiske fængsel! Grrrr!
Lyd|5|False|KLING !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Hvor har jeg dog været naiv !
Shichimi|2|True|Shhhhh Pepper!
Shichimi|3|False|Ikke så meget larm.
Pepper|4|False|Hvem dér?!
Shichimi|5|True|Shhhhh, sagde jeg!
Shichimi|6|True|Kom herhen.
Shichimi|7|False|Jeg er kommet for at hjælpe dig fri.
Lyd|8|False|Dzii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Jeg er... jeg er ked af hvad der er sket...
Shichimi|2|True|...du ved, med vores kamp.
Shichimi|3|True|Jeg...
Shichimi|4|False|Jeg var nødt til det.
Pepper|5|True|Bare rolig, det havde jeg forstået.
Pepper|6|False|Tak fordi du kom.
Shichimi|7|False|Denne magiske celle er virkelig stærk. De har givet dig den fulde pakke.
Pepper|8|False|Ha, ha!
Shichimi|9|False|Vær stille, de opdager os.
Rotte|10|True|SLURP
Rotte|11|True|SLURP
Rotte|12|False|SLURP
Shichimi|13|True|Ved du hvad?
Shichimi|14|True|Jeg er her også fordi, lige efter ceremonien, blev jeg en del af Wasabis indre kreds.
Shichimi|15|False|Og fik dermed kendskab til hendes planer...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|6|True|KLING !
Shichimi|7|True|KLING !
Shichimi|8|False|KLING !
Pepper|9|True|KLING !
Pepper|10|True|KLING !
Pepper|11|False|Det er forfærdeligt, Pepper.
Shichimi|12|True|Wasabi vil ganske enkelt dominere alle de andre magiske skoler ...
Shichimi|13|True|Hun tager afsted i morgen ved solopgang med en hær af hekse mod Qualicity...
Shichimi|14|False|Åh nej!
Shichimi|15|False|Koriander.
Lyd|16|False|Og hendes kongerige!
Pepper|17|True|Og Zombiah-magien!
Pepper|18|False|Vi skal advare hende så hurtigt som muligt.
Rotte|19|False|En pilot og en drage venter på platformen for at få os afsted herfra.
Carrot|20|False|Sådan! Endelig gik låsen op!
Rotte|21|False|Dzing !
Lyd|1|True|Godt klaret!
Lyd|2|True|Jeg henter lige Carrot og min hat.
Lyd|3|True|piiiiiiv
Lyd|4|True|K K K KSSS!
Lyd|5|False|piiiiiiiiiiiv!
Pepper|22|False|! ! !
Shichimi|23|False|! ! !
Vagt|24|True|VAGTER!
Vagt|25|True|ALARM!
Vagt|26|False|EN UBUDEN GÆST PRØVER AT SLIPPE FANGEN FRI!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Vi var så tæt på...
Pepper|2|False|Ja, så tæt på...
Pepper|3|False|Forresten, ved du hvorfor Wasabi har så meget imod mig?
Shichimi|4|True|Hun er bange for Kaosah-hekse, Pepper...
Shichimi|5|True|Og især for jeres kædereaktioner.
Shichimi|6|False|Det er ifølge hende en stor trussel for hendes planer.
Pepper|7|True|Ah, det, pfff...
Pepper|8|False|Det behøver hun ikke at bekymre sig om: jeg har aldrig klaret en eneste.
Shichimi|9|False|Virkelig ?
Pepper|10|False|Ja virkelig, hahaha !
Shichimi|11|True|Hihihi!
Shichimi|12|False|Hun er virkelig paranoid...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|True|Officer!
Kongen|2|False|Bekræfter du at dette er heksens tempel?
Officer|3|True|Højst sandsynligt, min herre!
Officer|4|False|Flere af vores informanter så hende på dette sted for nyligt.
Kongen|5|True|GRrrr...
Kongen|6|False|Så det er dér at denne trussel mod vores krigskunst og vores traditioner bor!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|False|Lad os fejre vores alliance ved at jævne templet med jorden.
Lyd|2|False|Tap
Fjende|3|False|G odt sagt !
Hær|4|True|Jaaer!
Hær|5|True|Yâaa!
Hær|6|True|Yaaa!
Hær|7|True|Jaaaaer!
Hær|8|True|Yihaaa!
Hær|9|True|Yuurr!
Hær|10|True|Yihaaa!
Hær|11|True|Yihaaa!
Hær|12|True|Jaaa!
Hær|13|False|Hurra!
Kongen|14|True|KATAPULTER!
Kongen|15|False|FYYYR!!!
Lyd|16|False|hôoVUUUUUUUUUU !
Lyd|17|True|Schwisss !
Lyd|18|False|Schwisss !
Kongen|19|False|TIL ANGREB!!!
Pepper|20|True|Hvad sker der?
Pepper|21|False|Et angreb?!
Lyd|22|True|B U M!
Lyd|23|False|B U U M!
Shichimi|24|False|Hvad?!
Lyd|25|True|BA~D U M!
Lyd|26|False|B U M!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HOST
Pepper|2|False|HOST! !
Pepper|3|False|Shichimi! Er du okay?
Shichimi|4|True|Ja, intet brækket!
Shichimi|5|True|Hvad med dig?
Shichimi|6|False|Og Carrot?
Pepper|7|False|Vi har det fint.
Pepper|8|True|Sikke noget...
Pepper|9|False|Jeg... kan ikke... tro det...
Shichimi|10|True|Hvor kommer de hære fra?!
Shichimi|11|True|Og med katapulter ?!
Shichimi|12|False|Hvad vil de os?!
Pepper|13|True|Det ved jeg ikke...
Pepper|14|False|Men jeg kender de to dér...
Shichimi|15|False|?
Shichimi|16|False|! ! !
Shichimi|17|False|Torreya, herhenne!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Ånderne være lovet! Du er i god behold.
Lyd|3|False|Tap
Torreya|4|True|Jeg blev så bekymret, da jeg fik at vide at de tog dig til fange!
Torreya|5|True|Og denne kamp!
Torreya|6|False|Sikke et kaos!
Shichimi|7|False|Torreya, det er så godt at se dig.
Pepper|8|True|Utroligt...
Pepper|9|True|Så denne drage-pilot er Shichimis kæreste...
Pepper|10|True|Jeg kan ikke forestille mig, hvad der ville være sket hvis jeg havde gjort det af med hende.
Pepper|11|True|Og hærene må have fulgt efter mig.
Pepper|12|True|Uden dem ville vi ikke være befriet.
Pepper|13|False|Det hele føles så forbundet ...
Pepper|14|False|...ÅH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Hvad er der med hende?
Shichimi|2|False|Pepper? Er alt okay?
Pepper|3|True|Ja, alt er fint.
Pepper|4|False|Det var bare noget der gik op for mig.
Pepper|5|True|Alle disse konsekvenser, direkte som indirekte, det er mine handlinger, mine valg...
Pepper|6|False|...På en måde: min egen kædereaktion!
Shichimi|7|True|Virkelig?
Shichimi|8|False|Det bliver du nødt til at forklare mig.
Torreya|9|True|Ikke mere snak, vi er midt i slagmarken.
Torreya|10|True|Vi kan snakke om det på flyveturen.
Torreya|11|False|Hop op, Pepper !
Shichimi|12|True|Torreya har ret.
Shichimi|13|False|Vi bliver nødt til at tage afsted til Qualicity.
Pepper|14|False|Nej, vent.
Pepper|15|True|Wasabis hær går snart til modangreb.
Pepper|16|True|Vi kan ikke bare lade dem slå hinanden ihjel.
Pepper|17|False|Jeg føler at det er mit ansvar at sætte en stopper for denne kamp.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Men hvordan?
Arra|2|True|Ja, hvordan vil du gøre det, heks?
Arra|3|False|Du har ikke genvundet meget Rea, det kan jeg mærke.
Pepper|4|True|Du har fuldkommen ret, men jeg har en trylleformular som kan ordne det hele.
Pepper|5|False|Jeg har kun brug for din Rea for at kunne nå ud til alle disse folk.
Arra|6|True|Give energi til en heks?
Arra|7|True|Det er forbudt!
Arra|8|False|GLEM DET! ALDRIG!
Pepper|9|False|Du vil så hellere lade et blodbad finde sted?
Torreya|10|True|Jeg beder dig, Arra. Gør en undtagelse. Disse piger og drager som kæmper, de forbliver vores skole, vores familie.
Torreya|11|False|Selv for dig.
Shichimi|12|False|Ja, be' om, Arra.
Arra|13|True|PFF! Fint!
Arra|14|False|Men på hendes eget ansvar!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|fs h h h h! !
Pepper|2|False|WOOAAA!
Pepper|3|False|Det er så sadan, drage-Rea er!
Shichimi|4|False|Pepper, hurtigt! Vi har ikke meget tid!
Pepper|5|True|Allus... !
Pepper|6|True|Yous... !
Pepper|7|True|Needus... !
Pepper|8|True|Is... !
Pepper|9|False|...LOVUS !
Lyd|10|False|Dzziooo! !

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|Fiizz!
Lyd|2|True|Dziing!
Lyd|3|True|Ffhii!
Lyd|4|True|Schii!
Lyd|5|True|Schsss!
Lyd|6|True|Fiizz!
Lyd|7|True|Dziing!
Lyd|8|True|Schii!
Lyd|9|True|Ffhii!
Lyd|10|False|Schsss!
Pepper|11|True|Denne formular var mit første forsøg, da jeg arbejdede på anti-krigs-formularer.
Pepper|12|True|Den laver fjender om til venner,
Pepper|13|False|vold om til kærlighed og medfølelse.
Shichimi|14|True|Wauw!
Shichimi|15|True|Det... det er genialt, Pepper!
Shichimi|16|False|De stopper med at kæmpe!
Torreya|17|False|Det virker!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Hey, hvad sker der?
Shichimi|2|False|Nogen af dem begynder at kysse... ?!
Torreya|3|True|Øhm... det skaber en del nye par det der!
Torreya|4|False|Var det meningen, Pepper?
Pepper|5|False|Øh, nej! Jeg tror at det er dragens Rea som overdoserede kærligheden i min formular.
Torreya|6|False|Haha, denne kamp går direkte ind i historiebøgerne!
Shichimi|7|True|Hi, hi!
Shichimi|8|False|Helt sikkert!
Pepper|9|False|Åh, hvor pinligt!
Skrift|10|False|- SLUT -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Den 15. december 2021 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Værktøj: Krita 5.0beta, Inkscape 1.1 on Kubuntu Linux 20.04 Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Vidste I det?
Pepper|3|False|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 1036 tilhængere!
Pepper|5|False|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|6|False|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|7|False|Gå på www.peppercarrot.com og få mere information!
Pepper|8|False|Tak!
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
